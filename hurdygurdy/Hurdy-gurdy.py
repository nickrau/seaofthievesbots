import discord
from discord.ext import commands
from discord.utils import get
import youtube_dl
import os

token = os.environ['HURDY_TOKEN']

client = discord.Client()
bot = commands.Bot(command_prefix='##')

@bot.event
async def on_ready():
    print("Logged on as {0}".format(bot.user))

@bot.command(brief='Play song', description='Tries to find the song you mentioned and than plays it.')
async def playYoutube(ctx, url: str):
    print("Command wakeUp triggered by", ctx.message.author)

    # Joining voice channel of player
    global voice
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()

    await voice.disconnect()

    if voice and voice.is_connected():
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
        print(f"{bot.user} has connected to {channel}\n")
    await ctx.send(f"Joining {channel}")

    song_there = os.path.isfile("song.mp3")
    try:
        if song_there:
            os.remove("song.mp3")
            print("Removed old song file")
    except PermissionError as p:
        print("Trying to remove old song file, but it is being used.")
        await ctx.send("ERROR: Music playing")
        return

    await ctx.send(f"Searching youtube video at {url}")

    ydl_opts = {
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        print("Downloading audio now...\n")
        ydl.download([url])

    for file in os.listdir('./'):
        if file.endswith(".mp3"):
            name = file
            print(f"Renamed File: {file}\n")
            os.rename(file, "song.mp3")

    voice.play(discord.FFmpegPCMAudio("song.mp3"), after=lambda e: print(f"{name} has finished playing"))
    voice.source = discord.PCMVolumeTransformer(voice.source)
    voice.source.volume = 0.07

    nname = name.rsplit("-", 2)
    await ctx.send(f"Playing: {nname}")
    print("Playing\n")


@bot.command(brief='Stops playing and leaves the voice channel.', description='Stops playing and leaves the voice channel.')
async def stop(ctx):
    channel = ctx.message.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)

    if voice and voice.is_connected():
        await voice.disconnect()
        print(f"{bot.user} leaving {channel}")
        await ctx.send(f"Leaving {channel}")
    else:
        print(f"{bot.user} trying to leave channel but was not in one")
        await ctx.send("I am not in a voice channel... I think.")

bot.run(token)