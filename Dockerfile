FROM python:3.9

RUN pip install --upgrade pip && \
    pip install discord.py

WORKDIR /usr/src/app

COPY ./mainthievesbot /usr/src/app/

ENTRYPOINT python MainThievesBot.py