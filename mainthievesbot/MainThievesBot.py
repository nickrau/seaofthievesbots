import discord
from discord.ext import commands
from os import listdir
import os
import asyncio

token = os.environ['MAINBOT_TOKEN']

client = discord.Client()
bot = commands.Bot(command_prefix='$')
islands = {}
wakeUpQueue = []

@bot.event
async def on_ready():
    print("Logged on as {0}".format(bot.user))
    for file in listdir('islands/'):
        islands[file.replace(".jpg", "")] = [file]


@bot.command(brief='Wakes up a member by spamming them with messages.', description='Wakes up a member by spamming them with messages.\nTo use: call the command and tag the member into it.')
async def wakeUp(ctx, name: discord.Member):
    print("Command wakeUp triggered by", ctx.message.author)
    amount = 10
    if len(wakeUpQueue) == 0:
        wakeUpQueue.append([ctx.message.author, name, amount, 1])
        await wakeUpQueueRun()
    else:
        playerfound = False
        for queue in wakeUpQueue:
            if queue[0] == ctx.message.author:
                playerfound = True
                # print("PLAYER FOUND")
                if queue[3] < 3:
                    # print("ADDING MESSAGES")
                    queue[2] += amount
                    queue[3] += 1
                else:
                    await ctx.channel.send("{} you can't send more than 30 messages at a time.\nPlease wait until your messages are send.".format(ctx.message.author.mention))
        if not playerfound:
            # print("NEW IN QUEUE")
            wakeUpQueue.append([ctx.message.author, name, amount, 1])
            await wakeUpQueueRun()


async def wakeUpQueueRun():
    # print(len(wakeUpQueue))
    while len(wakeUpQueue) > 0:
        for queue in wakeUpQueue:
            x = 0
            while x < queue[2]:
                await queue[1].send('Wake up you f*ckin bastard!!!! And get in the server!!!!')
                x+=1
            wakeUpQueue.remove(queue)

@bot.command(brief="Find an island.", description="Finds an island by typing command with name. Name is without spaces and capital letters. Example: $island smugglersbay")
async def island(ctx, islandname):
    print("Command island triggered by", ctx.message.author)

    if islandname in islands.keys():
        await ctx.channel.send(file=discord.File('islands/'+islands[islandname][0]))
    else:
        await ctx.channel.send("I don't know this island")

# Todo: Stuurt één embed en dan geeft die error
@bot.command(brief="Gives a list of all islands.", description="Gives a list of all the islands on Sea of Thieves.")
async def list_islands(ctx):

    total_islands = len(islands)
    current_embed = 0
    current_island = 0
    current_folder = 0
    embed = discord.Embed(title="All Islands Page:"+str(current_folder))

    for island in islands:
        embed.add_field(name=".", value=island, inline=False)

        if current_island == 25 or current_island == total_islands:
            ctx.channel.send(embed=embed)
            embed = discord.Embed(title="All Islands Page:"+str(current_folder))
            current_folder += 1

        current_island += 1
        current_embed += 1
    await ctx.channel.send(embed=embed)

bot.run(token)
